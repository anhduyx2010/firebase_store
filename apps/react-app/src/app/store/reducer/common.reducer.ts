import * as type from '../actions/user/user.type';

interface initStateInterface {
  loading: loadingStateEnum;
}

export enum loadingStateEnum {
  'idle',
  'loading',
}

const initState: initStateInterface = {
  loading: loadingStateEnum.idle,
};

const commonReducer = (state = initState, action) => {
  switch (action.type) {
    case type.USER_SIGN_UP:
      return {
        ...state,
        loading: loadingStateEnum.loading,
      };

    case type.USER_SIGN_UP_SUCCESS:
      return {
        ...state,
        loading: loadingStateEnum.idle,
      };

    case type.USER_SIGN_UP_FAILED:
      return {
        ...state,
        loading: loadingStateEnum.idle,
      };
    default:
      return state;
  }
};

export default commonReducer;
