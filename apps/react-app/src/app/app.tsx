import firebase from "firebase";
import { useEffect } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import { config } from "../config";
import Loading from "./components/Loading";
import AuthRoute from "./routes/AuthRoute";
import MainRoute from "./routes/MainRoute";
import { useSelector } from 'react-redux';

import { loadingStateEnum } from "./store/reducer/user.reducer";
import { RootState } from "./store";

export function App() {
  const loading = useSelector((state: RootState) => state.commonReducer.loading);

  useEffect(() => {
    firebase.initializeApp(config);
  }, [])
  return (
    <>
      {loading === loadingStateEnum.loading && <Loading />}
      <Router>
        <Switch>
          <Route path="/customer" component={AuthRoute} />
          <Route path="/" component={MainRoute} />
        </Switch>
      </Router>
    </>
  );
}

export default App;
