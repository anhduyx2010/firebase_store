import * as type from '../actions/user/user.type';

export const SIGN_UP = () => {
  return {
    type: type.USER_SIGN_UP,
  };
};

export const SIGN_UP_SUCCESS = (user) => {
  return {
    type: type.USER_SIGN_UP_SUCCESS,
    user: user,
  };
};

export const SIGN_UP_FAILED = (error) => {
  return {
    type: type.USER_SIGN_UP_FAILED,
    error: error,
  };
};
