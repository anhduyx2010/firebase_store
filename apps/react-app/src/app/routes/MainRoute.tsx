import { Route, Switch } from 'react-router-dom'
import React from 'react'
import Home from '../pages/Home'
import Header from '../components/Header'
import SignIn from '../pages/SignIn'
import SignUp from '../pages/SignUp'

export default function MainRoute() {
  return (
    <div>
      <Header />
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/signin" component={SignIn} />
        <Route path="/signup" component={SignUp} />
      </Switch>
    </div>
  )
}
