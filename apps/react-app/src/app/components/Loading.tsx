import React from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',

      position: 'fixed',
      top: '50%',
      left: '50%',
      transform: 'translate(-50%, -50%)',
      margin: 'auto',

      zIndex: 4,

      '& > * + *': {
        marginLeft: theme.spacing(2),
      },
    },

    overlay: {
      background: 'rgba(0,0,0,0.5)',
      width: '100vw',
      height: '100vh',
      position: 'fixed',
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      zIndex: 3
    }
  }),
);

export default function Loading() {
  const classes = useStyles();

  return (
    <>
      <div className={classes.overlay}></div>
      <div className={classes.root}>
        <CircularProgress />
      </div>
    </>
  );
}
