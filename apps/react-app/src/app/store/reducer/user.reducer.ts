import * as type from '../actions/user/user.type';

interface initStateInterface {
  loading: loadingStateEnum;
  success: false | true;
  error: string | null;
}

export enum loadingStateEnum {
  'idle',
  'loading',
}

const initState: initStateInterface = {
  loading: loadingStateEnum.idle,
  success: false,
  error: null,
};

const userReducer = (state = initState, action) => {
  switch (action.type) {
    case type.USER_SIGN_UP:
      return {
        ...state,
        loading: loadingStateEnum.loading,
      };

    case type.USER_SIGN_UP_SUCCESS:
      return {
        ...state,
        success: true,
        loading: loadingStateEnum.idle,
        user: action.user,
      };

    case type.USER_SIGN_UP_FAILED:
      return {
        ...state,
        success: false,
        loading: loadingStateEnum.idle,
        error: action.error,
      };
    default:
      return state;
  }
};

export default userReducer;
