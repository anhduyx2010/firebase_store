import { useState } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { Link } from 'react-router-dom';
import firebase from "firebase/app";
import "firebase/auth";
import { useForm } from 'react-hook-form';
import { FormHelperText } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux'
import * as effect from '../store/effects/user.effect'
import Notification from '../components/Notifications'
import { RootState } from '../store';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function SignUpForm() {
  const classes = useStyles();

  const [signUpSuccess, setSignUpSuccess] = useState(null);
  const [signUpError, setSignUpError] = useState(null);
  const [openNotifictionSuccess, setOpenNotifictionSuccess] = useState(false);
  const [openNotifictionError, setOpenNotifictionError] = useState(false);

  const dispatch = useDispatch()
  const user = useSelector((state: RootState) => state.userReducer);

  interface IFormInput {
    email: string;
    password: string;
  }

  const onSubmit = (data: IFormInput) => {
    const { email, password } = data
    dispatch(effect.SIGN_UP());
    firebase.auth().createUserWithEmailAndPassword(email, password)
      .then((userCredential) => {
        const user = userCredential.user;
        dispatch(effect.SIGN_UP_SUCCESS(user));
        setSignUpSuccess(user);
        setOpenNotifictionSuccess(true);

      })
      .catch((error) => {
        const errorMessage = error.message;
        dispatch(effect.SIGN_UP_FAILED(errorMessage));
        setSignUpError(errorMessage);
        setOpenNotifictionError(true);
      });
  }

  const { register, handleSubmit, formState: { errors } } = useForm({
    mode: "onChange"
  })

  return (
    <Container component="main" maxWidth="xs">
      {signUpSuccess && <Notification notification_duration={3000} notification_state="success" open={openNotifictionSuccess}>
        Sign Up is Success !
      </Notification>}

      {signUpError && <Notification notification_duration={3000} notification_state="error" open={openNotifictionError}>
        {signUpError}
      </Notification>}

      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign up
        </Typography>
        <form className={classes.form} noValidate onSubmit={handleSubmit(onSubmit)}>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="email"
            label="Email"
            id="email"
            autoFocus
            {...register('email', { required: "Email is Required" })}
          />
          {errors.email && <FormHelperText error={true} about={errors.email.message}>{errors.email.message}</FormHelperText>}
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            {...register('password', { required: "Password is required", minLength: { value: 6, message: "Password should be at least 6 characters" } })}
          />
          {errors.password && errors.password.type === "required" && <FormHelperText error={true} about={errors.password.message}>{errors.password.message}</FormHelperText>}
          {errors.password && errors.password.type === "minLength" && <FormHelperText error={true} about={errors.password.message}>{errors.password.message}</FormHelperText>}
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Sign In
          </Button>
          <Grid container>
            <Grid item>
              <Link to="/signin">
                "Don't have an account?
                Sign In"
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
    </Container>
  );
}
