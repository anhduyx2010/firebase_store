import { combineReducers } from 'redux';
import userReducer from './user.reducer';
import commonReducer from './common.reducer';

const RootReducer = combineReducers({
  userReducer,
  commonReducer,
});

export default RootReducer;
